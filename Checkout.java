

import java.util.ArrayList;

public class Checkout {
	ArrayList <DessertItem> Desserts;
	public Checkout() {
	Desserts=new ArrayList<DessertItem>();	
	}
	public void clear() {
			Desserts.clear();
	}
	public void enterItem(DessertItem Dessert) {
		Desserts.add(Dessert);
	}
	public int numberofItems() {
		int number;
		number=Desserts.size();
		return number;
	}
	public int totalCost() {
		int cost = 0;
		for (int i=0; i<Desserts.size(); i++) {
			cost +=Desserts.get(i).getCost();
		}
		return cost;
	}
	public int totalTax() {
		int tax=0;
		tax=(int) Math.round(this.totalCost()*0.065);
		return tax;	
	}
	public  java.lang.String toString(){
		String s="";
		s +="  ";
		s +="M & M Dessert Shoppe" +"\n";
		s +="--------------------" +"\n";
		
		for (int i=0;i<Desserts.size();i++) {
			if(Desserts.get(i)  instanceof Candy ) {
				
				s +=DessertShoppe.cents2dollarsAndCents ((int) ((((Candy)Desserts.get(i)).getWeight())*100))+" "+"lbs.";
				s +=" "+"@"+ DessertShoppe.cents2dollarsAndCents((((Candy)Desserts.get(i)).getCandyPrice()))+" "+"/lb."+"\n";
				s += Desserts.get(i).getName()+"   "+"       "+ DessertShoppe.cents2dollarsAndCents((((Candy)Desserts.get(i)).getCost()))+"\n";	
			}
			else if(Desserts.get(i)  instanceof Cookie ) {
				
				s += ((Cookie)Desserts.get(i)).getNumber();
				s +=" "+"@"+ DessertShoppe.cents2dollarsAndCents((((Cookie)Desserts.get(i)).getPricePerDozen()))+" "+"/dz."+"\n";
				s += Desserts.get(i).getName()+"   "+"   "+ DessertShoppe.cents2dollarsAndCents((((Cookie)Desserts.get(i)).getCost()))+"\n";	
			}
         else if(Desserts.get(i)  instanceof IceCream ) {
				
				s += Desserts.get(i).getName()+"   "+"       "+ DessertShoppe.cents2dollarsAndCents((((IceCream)Desserts.get(i)).getCost()))+"\n";	
			}
         else if(Desserts.get(i)  instanceof Sundae ) {
				s +=((Sundae)Desserts.get(i)).getTopingName();
				s += Desserts.get(i).getName()+"   "+"    "+ DessertShoppe.cents2dollarsAndCents((((Sundae)Desserts.get(i)).getCost()))+"\n";	
			}
			
		}
		s+="\n";
		s +="Tax "+"                "+ DessertShoppe.cents2dollarsAndCents(this.totalTax())+"\n";
		s +="Total cost"+"         "+DessertShoppe.cents2dollarsAndCents(this.totalCost()+this.totalTax())+"\n";
		
		return s;
		
	}

}
