

public class Sundae extends IceCream {
	private String topping;
	private int toppingprice;

	public Sundae(String name, int price , String topp , int toppprice) {
		super(name, price);
		topping=topp;
		toppingprice=toppprice;
	
	}
	
	public int getCost() {
		return super.getCost()+toppingprice;
		
	}
	
	public String getTopingName() {
		return topping; 
	}	
}
