

public class Candy extends DessertItem {
	
private double weight;
private int candyprice;
private int foredit;

public Candy (String name,double we1,int price ) {
	super(name);
	weight=we1;
	candyprice=price;
}

public double getWeight() {
	return weight;	
}

public int getCandyPrice() {
	return candyprice;	
}
public void setWeight(double we) {
	weight=we;
}
public void setCandyPrice(int price) {
	candyprice=price;
}
	@Override
	public int getCost() {
	int totalprice =(int) Math.round(weight*candyprice);
		return totalprice;
	}
	

}
