

public class Cookie extends DessertItem {
	
	private int number;
	private int priceperdozen;
	
	public Cookie(String name, int num, int priceperdoz) {
		super(name);
		number=num;
		priceperdozen=priceperdoz;	
	}
	public int getNumber() {
		return number;
	}
	public int getPricePerDozen() {
		return priceperdozen;
	}

	@Override
	public int getCost() {
		
		int cost=Math.round(priceperdozen * number/12);
		return cost;
	}

}
